import React from 'react';
import './index.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock } from '@fortawesome/free-regular-svg-icons';

export const Action = ({ text, date }) => {
  return (
    <div className="recent-actions-action">
      <FontAwesomeIcon className="action__icon"
        icon={ faClock } />
      <div className="action-content">
        <p className="content__text">{ text }</p>
        <p className="content__date">{ date.toDateString() }</p>
      </div>
    </div>
  );
}
