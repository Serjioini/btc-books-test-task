import React from 'react';
import './index.scss';
import { Action } from './Action';

export const RecentActions = ({ actions }) => {
  return (
    <div className="sidebar-recent-actions">
      {
        actions.map(action =>
          <Action
            key={ action.text }
            text={ action.text }
            date={ action.date } />)
      }
    </div>
  );
}
