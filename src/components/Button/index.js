import React from 'react';
import './index.scss';

export const Button = ({ name, action, className }) => {
  return (
    <span className={'button'
      + (className !== '' ? ' ' + className : '') }
      onClick={ action }>{ name }</span>
  );
}
