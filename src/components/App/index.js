import React, { Component } from 'react';
import './index.scss';
// data
import defaultBooks from './Books.json';
// essentials
import { Book } from './Book';
// components
import { Button } from '../Button';
import { Menu } from '../Menu';
import { RecentActions } from '../RecentActions';
import { Filters } from '../Filters';
import { Books } from '../Books';
import { BookModal } from '../BookModal';
import { MessageModal } from '../MessageModal';
// fontawesome
import { library }  from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

library.add(far, fas);

class App extends Component {

  menuItems: Array = [
    {
      id: 0,
      name: 'Now Reading',
      title: 'Now Reading',
      icon: 'book'
    },
    {
      id: 1,
      name: 'Browse',
      title: 'Browse Available Books',
      icon: 'globe-americas'
    },
    {
      id: 2,
      name: 'Buy Books',
      title: 'Buy Books',
      icon: 'shopping-cart'
    },
    {
      id: 3,
      name: 'Favorite Books',
      title: 'Favorite Books',
      icon: 'star'
    },
    {
      id: 4,
      name: 'White List',
      title: 'White List',
      icon: 'list'
    }
  ];
  books: Array = [];
  groups: Array = [
    {
      id: 'all books',
      name: 'all books',
      condition: book => true,
    },
    {
      id: 'most recent',
      name: 'most recent',
      condition: book => {
        let date = new Date(book.date);
        return date.getFullYear() === 2018
          && date.getMonth() >= 7
      }
    },
    {
      id: 'most popular',
      name: 'most popular',
      condition: book => book.stars > 4
    },
    {
      id: 'free books',
      name: 'free books',
      condition: book => book.price === 0
    },
  ];

  constructor(props) {
    super(props);
    // menu items
    this.onMenuItemChanged = this.onMenuItemChanged.bind(this);
    // filters
    this.onGroupChanged = this.onGroupChanged.bind(this);
    this.onSearchStringChanged = this.onSearchStringChanged.bind(this);
    // actions
    this.addAction = this.addAction.bind(this);
    // book modal
    this.onOpenBookReader = this.onOpenBookReader.bind(this);
    this.onOpenBookCreator = this.onOpenBookCreator.bind(this);
    this.onCloseBookModal = this.onCloseBookModal.bind(this);
    this.onCreateBook = this.onCreateBook.bind(this);
    // message modal
    this.onOpenMessageModal = this.onOpenMessageModal.bind(this);
    this.onCloseMessageModal = this.onCloseMessageModal.bind(this);

    let books = JSON.parse(localStorage.getItem('books'));
    books = books || defaultBooks;
    books = books.map(book => new Book(book));
    this.filteredBooks = books;

    this.state = {
      currentMenuItem: this.menuItems[1],
      currentGroup: this.groups[0],
      searchString: '',
      recentActions: [],
      books,
      bookModal: {
        active: false,
        mode: 'creator', // 'reader', 'updater?', 'deleter?'
        book: new Book()
      },
      messageModal: {
        active: false,
        text: ''
      }
    };
  }

  render() {
    let menuItems = this.menuItems.map(item => {
      return { ...item, active: item.id === this.state.currentMenuItem.id };
    });
    let groups = this.groups.map(group => ({
      ...group,
      active: group.id === this.state.currentGroup.id
    }));

    return (
      <div className="app">
        <div className="app-sidebar">
          <div className="sidebar-wrapper">
            <Button className="sidebar__button"
              name="+ Add A Book"
              action={ this.onOpenBookCreator } />
          </div>
          <Menu
            items={ menuItems }
            onMenuItemChanged={ this.onMenuItemChanged } />
          <RecentActions actions={ this.state.recentActions }/>
        </div>
        <div className="app-main">
          <h1 className="main-title">{ this.state.currentMenuItem.title }</h1>
          <Filters
            groups={ groups }
            onGroupChanged={ this.onGroupChanged }
            onSearchStringChanged={ this.onSearchStringChanged } />
          <Books
            books={ this.filteredBooks }
            onOpenBookReader={ this.onOpenBookReader }/>
        </div>

        {
          this.state.bookModal.active &&
          <BookModal
            mode={ this.state.bookModal.mode }
            book={ this.state.bookModal.book }
            onCloseBookModal={ this.onCloseBookModal }
            onCreateBook={ this.onCreateBook } />
        }
        {
          this.state.messageModal.active &&
          <MessageModal
            text={ this.state.messageModal.text }
            onClose={ this.onCloseMessageModal } />
        }
      </div>
    );
  }

  // -- update cycle
  shouldComponentUpdate(nextProps, nextState) {
    let { currentGroup, searchString, books } = nextState;

    let searchStringChanged = searchString !== this.state.searchString;
    let groupChanged = currentGroup.id !== this.state.currentGroup.id;
    let booksChanged = books.length !== this.state.books.length;
    if (groupChanged || searchStringChanged || booksChanged) {
      this.filteredBooks = nextState.books
        .filter(currentGroup.condition)
        .filter(book => {
          let name = book.name.toLowerCase();
          return name.indexOf(searchString) !== -1;
        });
    }

    return true;
  }

  // menu items
  onMenuItemChanged(menuItem) {
    this.setState({ currentMenuItem: menuItem });
  }
  // actions
  addAction(text) {
    this.setState((state, props) => {
      let created = {
        id: -1,
        text,
        date: new Date(Date.now())
      };

      if (state.recentActions.length !== 0) {
        let last = state.recentActions.pop();
        created.id = last.id + 1;
        return {
          recentActions: [ ...state.recentActions, last, created ]
        };
      } else {
        created.id = 0;
        return {
          recentActions: [ created ]
        };
      }
    })
  };
  // filters
  onGroupChanged(group) {
    this.setState({ currentGroup: group });
  }
  onSearchStringChanged(searchString) {
    this.setState({ searchString });
  }
  // book modal
  onOpenBookReader(book) {
    this.setState({
      bookModal: {
        active: true,
        mode: 'reader',
        book: book
      }
    });
  }
  onOpenBookCreator() {
    this.setState({
      bookModal: {
        active: true,
        mode: 'creator',
        book: new Book()
      }
    });
  }
  onCloseBookModal() {
    this.setState({
      bookModal: { active: false }
    });
  }
  onCreateBook(book) {
    if (book === null) {
      this.addAction('Book not added');
    } else {
      this.setState(state => {
        // set real id
        let maxId = state.books.reduce((max, book) => {
          return max < book.id ? book.id : max;
        }, 0);
        book.id = maxId + 1;
        // save book
        let books = [ ...state.books, book ];
        localStorage.setItem('books', JSON.stringify(books));

        return {
          books,
          bookModal: { active: false }
        };
      });
      this.addAction('Book successfully added!')
      this.onOpenMessageModal(`Book ${book.name} successfully added!`);
    }
  }
  // message modal
  onOpenMessageModal(text) {
    this.setState({ messageModal: { text, active: true } });
  }
  onCloseMessageModal() {
    this.setState({ messageModal: { active: false } });
  }

}

export default App;
