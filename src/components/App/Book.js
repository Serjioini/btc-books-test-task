export class Book {

  constructor(data = {}) {
    this.isValid = this.isValid.bind(this);

    // required
    this.id = data.id || -1;
    this.name = data.name || '';
    this.author = data.author || '';
    this.publisher = data.publisher || '';
    this.isbn = data.isbn || '';
    this.date = new Date(Date.parse(data.date || '2018-01-01'));
    this.stars = data.stars || 0;
    this.price = data.price || 0;
    // not required
    this.summary = data.summary || '';
    this.paperback = data.paperback || '';
    this.genre = data.genre || '';
    this.img = data.img || 'default_image.jpg';
  }

  isValid() {
    let isbnConsistsOfNumbers = this.isbn.split('')
      .some(ch => '1234567890'.indexOf(ch) === -1);
    return this.name !== ''
      && this.author !== ''
      && this.publisher !== ''
      && this.isbn !== '' && !isbnConsistsOfNumbers;
  }

  static mapToModalBook(book) {
    let result = [
      {
        id: 'general',
        name: 'General',
        icon: 'align-center',
        fields: [
          {
            id: 'title',
            name: 'title',
            displayName: 'Title',
            value: book.name || '',
            required: true,
            errors: [],
            handler: value => [],
            size: 2
          },
          {
            id: 'author',
            name: 'author',
            displayName: 'Author',
            value: book.author || '',
            required: true,
            errors: [],
            handler: value => [],
            size: 2
          },
          {
            id: 'publisher',
            name: 'publisher',
            displayName: 'Publisher',
            value: book.publisher || '',
            required: true,
            errors: [],
            handler: value => [],
            size: 2
          },
          {
            id: 'paperback',
            name: 'paperback',
            displayName: 'Paperback',
            value: book.paperback || '',
            required: false,
            errors: [],
            handler: value => [],
            size: 1
          },
          {
            id: 'isbn',
            name: 'isbn',
            displayName: 'ISBN',
            value: book.isbn || '',
            required: true,
            errors: [],
            handler: value => {
              let errors = [];
              if (value.split('').some(ch => '1234567890'.indexOf(ch) === -1)) {
                errors.push('Must be consists of numbers');
              }
              return errors;
            },
            size: 1
          },
          {
            id: 'summary',
            name: 'summary',
            displayName: 'Summary',
            value: book.summary || '',
            required: false,
            errors: [],
            handler: value => [],
            size: 2
          },
        ]
      },
      {
        id: 'genre',
        name: 'Genre',
        icon: 'tags',
        fields: [
          {
            id: 'genre',
            name: 'genre',
            displayName: 'Genre',
            value: book.genre || '',
            required: false,
            errors: [],
            handler: value => [],
            size: 2
          },
        ]
      },
      {
        id: 'poster',
        name: 'Poster',
        icon: 'image',
        fields: []
      },
      {
        id: 'info',
        name: 'Info',
        icon: 'info-circle',
        fields: []
      },
    ];
    return result;
  }
  static mapToNormalBook(book) {
    let result = new Book();
    result.name = book[0].fields[0].value;
    result.author = book[0].fields[1].value;
    result.publisher = book[0].fields[2].value;
    result.paperback = book[0].fields[3].value;
    result.isbn = book[0].fields[4].value;
    result.summary = book[0].fields[5].value;
    result.genre = book[1].fields[0].value;
    return result;
  }

  static checkField(field) {
    // standard handler
    let errors = [];
    if (field.required && field.value === '') {
      errors = [ 'Empty value' ];
    }
    // and custom hundler
    let customErrors = field.handler(field.value);

    return [...errors, ...customErrors];
  }

}
