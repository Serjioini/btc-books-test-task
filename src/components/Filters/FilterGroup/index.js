import React from 'react';
import './index.scss';

export const FilterGroup = ({ name, active, onChanged }) => {
  return (
    <span className={ 'filter-by-group'
      + (active ? ' filter-by-group_active' : '') }
      onClick={ onChanged }>{ name }</span>
  );
}
