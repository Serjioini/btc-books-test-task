import React from 'react';
import './index.scss';
import { FilterGroup } from './FilterGroup';
import { SearchLine } from './SearchLine';

export const Filters = ({ groups, onGroupChanged, onSearchStringChanged }) => {
  return (
    <div className="main-filters">
      <div className="filters-by-group">
        {
          groups.map(group =>
            <FilterGroup
              key={ group.id }
              name={ group.name }
              active={ group.active }
              onChanged={ () => onGroupChanged(group) } />
          )
        }
      </div>
      <SearchLine onChanged={ onSearchStringChanged } />
    </div>
  );
}
