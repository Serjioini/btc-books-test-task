import React from 'react';
import './index.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const SearchLine = ({ onChanged }) => {
  return (
    <div className="filters-search-line">
      <input className="search-line__input" type="text" placeholder="Enter Keywords"
        onInput={ (e) => onChanged(e.target.value) } />
      <FontAwesomeIcon className="search-line__icon" icon="search" />
    </div>
  );
}
