import React from 'react';
import './index.scss';
import { Tab } from './Tab';

export const Tabs = ({ tabs, onTabChange }) => {
  return (
    <div className="popup-tabs">
      {
        tabs.map(tab =>
          <Tab
            key={ tab.id }
            name={ tab.name }
            icon={ tab.icon }
            active={ tab.active }
            onChange={ () => onTabChange(tab) } />
        )
      }
    </div>
  );
}
