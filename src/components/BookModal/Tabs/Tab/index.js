import React from 'react';
import './index.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const Tab = ({ name, icon, active, onChange }) => {
  let classNames = {
    tab: 'tabs-tab' + (active ? ' tabs-tab_active' : '')
  };

  return (
    <div className={ classNames.tab } onClick={ onChange }>
      <FontAwesomeIcon className="tab__icon" icon={ icon } />
      <span className="tab__name">{ name }</span>
    </div>
  );
}
