import React from 'react';
import './index.scss';
import { Button } from '../../Button';

export const Footer = ({ mode, onClose, onCreateBook }) => {
  let classNames = {
    buttonCancel: 'footer__button' + (mode === 'reader' ? '' : ' footer__button_secondary'),
    buttonCreateBook: 'footer__button' + (mode === 'reader' ? ' footer__button_hidden' : '')
  };

  return (
    <div className="popup-footer">
      <Button className={ classNames.buttonCancel }
        name={ 'close' }
        action={ onClose } />
      <Button className={ classNames.buttonCreateBook }
        name={ 'add book' }
        action={ onCreateBook } />
    </div>
  );
}
