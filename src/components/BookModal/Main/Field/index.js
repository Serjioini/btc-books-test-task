import React from 'react';
import './index.scss';

export const Field = ({ mode, id, value, displayName, required, onChanged, errors, size }) => {
  let classNames = {
    field: 'main-field main-field_size_' + (size ? size : '2'),
    reader: {
      fieldName: 'field__name',
      fieldText: 'field__text'
    },
    creator: {
      fieldName: 'field__name' + (required ? ' field__name_required' : ''),
      fieldInput: 'field__input' + (errors.length ? ' field__input_invalid' : '')
    },
  };

  return (
    mode === 'reader' && (
      <div className={ classNames.field }>
        <label className={ classNames.reader.fieldName }>{ displayName }</label>
        <p className={ classNames.reader.fieldText }>{ value }</p>
      </div>
    ) ||
    mode === 'creator' && (
      <div className={ classNames.field }>
        <label className={ classNames.creator.fieldName }>{ displayName }</label>
        <input className={ classNames.creator.fieldInput }
          value={ value }
          placeholder={ 'Enter ' + displayName }
          onChange={ e => onChanged(id, e.target.value) }
          onBlur={ e => onChanged(id, e.target.value) } />
        <div className="field__errors">{ errors.length !== 0 ? errors[0] : '' }</div>
      </div>
    )
  );
};
