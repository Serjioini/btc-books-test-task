import React from 'react';
import './index.scss';
import { Field } from './Field';

export const Main = ({ fields, mode, onFieldValueChanged }) => {
  return (
    <div className="popup-main">
      {
        fields.map(field => {
          return (
            <Field
              key={ field.id }
              mode={ mode }
              id={ field.id }
              value={ field.value }
              displayName={ field.displayName }
              required={ field.required }
              errors={ field.errors }
              size={ field.size }
              onChanged={ onFieldValueChanged } />
          );
        })
      }
    </div>
  );
}
