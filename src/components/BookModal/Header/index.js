import React from 'react';
import './index.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const Header = ({ onClose }) => {
  return (
    <div className="book-creator-header">
      <h1 className="header__text">Add New Book</h1>
      <FontAwesomeIcon className="header__close-button"
        icon="times"
        onClick={ onClose }/>
    </div>
  );
}
