import React, { Component } from 'react';
import './index.scss';
// essentials
import { Book } from '../App/Book';
// components
import { Header } from './Header';
import { Main } from './Main';
import { Tabs } from './Tabs';
import { Footer } from './Footer';

export class BookModal extends Component {

  constructor(props) {
    super(props);
    this.onFieldValueChanged = this.onFieldValueChanged.bind(this);
    this.onTabChanged = this.onTabChanged.bind(this);
    this.onCreateBook = this.onCreateBook.bind(this);
    this.onClose = this.onClose.bind(this);

    let tabs = Book.mapToModalBook(props.book);
    this.state = { tabs, currentTab: tabs[0] };
  }

  render() {
    let fields: Array = this.state.currentTab.fields;
    let tabs = this.state.tabs.map(tab => {
      let active = tab.id === this.state.currentTab.id;
      return { ...tab, active };
    });

    return (
      <div className="book-creator">
        <div className="book-creator-popup">
          <Header onClose={ this.onClose } />
          <div className="popup-wrapper">
            <Tabs
              tabs={ tabs }
              onTabChange={ this.onTabChanged } />
            <Main
              fields={ fields }
              mode={ this.props.mode }
              onFieldValueChanged={ this.onFieldValueChanged } />
          </div>
          <Footer
            mode={ this.props.mode }
            onClose={ this.onClose }
            onCreateBook={ this.onCreateBook } />
        </div>
      </div>
    );
  }

  onFieldValueChanged(id, value) {
    this.setState(state => {
      // refresh target field in current tab
      let fields = state.currentTab.fields.map(field => {
        if (field.id === id) {
          let updated = { ...field, value };
          updated.errors = Book.checkField(updated);
          return updated;
        }
        return field;
      });
      // save current tab in tabs
      let currentTab = { ...state.currentTab, fields };
      let tabs = state.tabs.map(tab => {
        return tab.id === currentTab.id ? currentTab : tab;
      });

      return { tabs, currentTab };
    });
  }
  onTabChanged(tab) {
    this.setState({ currentTab: tab });
  }
  onCreateBook() {
    let book = Book.mapToNormalBook(this.state.tabs);
    if (book.isValid()) {
      // receive valid book
      this.props.onCreateBook(book);
    } else {
      // receive errors
      this.setState(state => {
        let tabs = state.tabs.map(tab => {
          let fields = tab.fields.map(field => {
            let errors = Book.checkField(field);
            return { ...field, errors };
          });
          return { ...tab, fields };
        });
        let currentTab = tabs.reduce((found, tab) => {
          return tab.id === state.currentTab.id ? tab : found;
        }, null);
        return { tabs, currentTab };
      });
    }
  }
  onClose() {
    this.props.onCloseBookModal();
  }

}
