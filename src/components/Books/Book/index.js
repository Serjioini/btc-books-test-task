import React from 'react';
import './index.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-regular-svg-icons';

export const Book = ({ name, author, stars, img, onClick }) => {
  const img_resource = img ? require('../../../img/' + img) : null;
  return (
    <div className="books-book">
      <img className="book__img" src={ img_resource ? img_resource : '' } alt=""
        onClick={ onClick } />
      <h2 className="book__name">{ name }</h2>
      <span className="book__author">{ `by ${author}` }</span>
      <div className="book-stars">{ Book.stars(stars) }</div>
    </div>
  );
}

Book.stars = (n) => {
  let full = [];
  let half = [];
  let empty = [];
  if (n - Math.floor(n) > 0.4) {
    half.push( <FontAwesomeIcon key={ 0 } icon="star-half-alt" /> );
    n = Math.floor(n);
  }
  for (let i = 0; i < n; ++i) {
    full.push( <FontAwesomeIcon key={ 1 + 0.1 * i } icon="star" /> );
  }
  const EMPTY_LENGTH = 5 - full.length - half.length;
  for (let i = 0; i < EMPTY_LENGTH; ++i) {
    empty.push( <FontAwesomeIcon key={ 2 + 0.1 * i } icon={ faStar } /> );
  }

  return [...full, ...half, ...empty];
}
