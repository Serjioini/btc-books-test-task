import React from 'react';
import './index.scss';
import { Book } from './Book';

export const Books = ({ books, onOpenBookReader }) => {
  return (
    <div className="main-books">
      {
        books.map(book =>
          <Book
            key={ book.id }
            name={ book.name }
            author={ book.author }
            stars={ book.stars }
            img={ book.img }
            onClick={ () => onOpenBookReader(book) } />
        )
      }
    </div>
  );
}
