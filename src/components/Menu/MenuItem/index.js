import React from 'react';
import './index.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const MenuItem = ({ name, active, icon, onClick }) => {
  return (
    <div className={ 'menu-menu-item'
      + (active ? ' menu-menu-item_active' : '') }
      onClick={ onClick }>
      <FontAwesomeIcon icon={ icon } />
      <span className="menu-item__name">{ name }</span>
    </div>
  );
}
