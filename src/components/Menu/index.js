import React from 'react';
import './index.scss';
import { MenuItem } from './MenuItem';

export const Menu = ({ items, onMenuItemChanged }) => {
  return (
    <div className="sidebar-menu">
      {
        items.map(item =>
          <MenuItem
            key={ item.id }
            name={ item.name }
            active={ item.active }
            icon={ item.icon }
            onClick={ () => onMenuItemChanged(item) } />
        )
      }
    </div>
  );
}
