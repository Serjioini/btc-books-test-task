import React from 'react';
import './index.scss';
import { Button } from '../Button';

export const MessageModal = ({ text, onClose }) => {
  return (
    <div className="message-modal">
      <div className="message-modal-main">
        <h1 className="main__text">{ text }</h1>
      </div>
      <div className="message-modal-footer">
        <Button className="footer__button"
          name="close"
          action={ onClose } />
      </div>
    </div>
  );
}
